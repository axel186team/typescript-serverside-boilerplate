import {app} from "./app";

process.on('unhandledRejection', (reason, p) =>
  console.error('Unhandled Rejection at: Promise ', p, reason)
);

console.log('App is ready and running.', app);
